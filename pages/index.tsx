import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'

const Home: NextPage = () => {
  return (
    <div className=" ">
      <Head>
        <title>Emporeus</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <header className='shadow-[0_10px_20px_rgba(0,0,0,0.08)]'>
        <div className="container max-w-6xl	mx-auto">
          <div className='flex justify-between items-center'>
            <a href="" className='py-2'>
              <Image src="/static/images/emporeuspng.png" alt="Emporeus" width={150} height={75} />
            </a>
            <nav>
              <ul className="flex items-center menu">
                <li><a href="index.html" className='px-4 block hover:text-white  md:mr-3 py-9 no-underline hover:bg-yellow-400'><span>Home</span></a></li>
                <li><a href="about.html" className='px-4 block hover:text-white  md:mr-3 py-9 no-underline hover:bg-yellow-400' ><span>About</span></a></li>
                <li><a href="productspage.html" className='px-4 block hover:text-white  md:mr-3 py-9 no-underline hover:bg-yellow-400' ><span>Products</span></a></li>
                <li><a href="contactuspage.html" className='px-4 block hover:text-white  md:mr-3 py-9 no-underline hover:bg-yellow-400' ><span>Contact</span></a></li>
              </ul>
            </nav>
          </div>
        </div>
      </header>
      {/* welcome section */}
      <section className='aboutSection py-24 bg-yellow-500'>
        <div className="container max-w-6xl	mx-auto text-center">
          <h2 className="text-4xl font-bold uppercase text-center mb-3">Welcome to Emporeus</h2>
          <h5 className="text-xl font-['serif'] text-center text-white mb-4 italic">-Premium quality products-</h5>
          <div className="content font-['serif'] text-2xl text-center italic max-w-3xl mx-auto mb-4">
            Emporeus Inc is known as the leading Distributor and Exporter of Food Products and much more. These
            products are highly well-liked for their rich taste, longer shelf life, freshness, high nutrient value
            and purity.
          </div>
          <button className='bg-white p-3 mx-auto motion-safe:hover:-translate-y-0.5 motion-safe:transition'>Read more</button>
        </div>

      </section >
      {/* product section */}
      <section className="py-24">
        <div className="container max-w-6xl	mx-auto ">
          <h2 className="text-4xl font-bold font-['serif'] mb-4 text-center ">Products</h2>
          <h5 className="text-lg font-['serif'] text-center mb-8 italic">
            We bring happiness to every individual with our high-quality products.
          </h5>
          <div className="productContainer">
            <div className="grid grid-cols-3 gap-4">
              {[0, 1, 2].map((x, index) => (
                <a className='productCard text-center hover:shadow-[0_0_20px_5px_rgb(0,0,0,0.05)] transition p-5' key={index}>
                  <h3 className="text-xl font-bold mb-3 relative inline-block
                after:content-[''] after:absolute after:block
                 after:h-[9px] after:w-full after:bg-[#ffc107]
                 after:bottom-1
                 after:left-0
                 after:-z-10">Chocos</h3>
                  <h5 className='text-sm opacity-80'>
                    Coupled with health benefits of chocoflakes correct amount of chocolate helps to lower blood pressure and cholestrol level.
                  </h5>
                  <img src="/static/images/choco.jpg" alt="Choco" />
                </a>
              ))}



            </div>
          </div>
        </div>
      </section >
      <footer>

      </footer>
    </div >
  )
}

export default Home
